function calculateShipping(weight, method) {
  let shippingFee = 0;

  if (weight <= 0.5) {
    shippingFee = 90;
  } else if (weight <= 1) {
    shippingFee = 120;
  } else if (weight <= 3) {
    shippingFee = 250;
  } else if (weight <= 5) {
    shippingFee = 380;
  } else if (weight <= 10) {
    shippingFee = 550;
  } else {
    return "More than 10 kilos must not be allowed";
  }

  switch (method) {
    case "local":
      shippingFee *= 1.0;
      break;
    case "overseas":
      shippingFee *= 1.5;
      break;
    default:
      return "Invalid shipping method";
  }

  return `The total shipping fee is P${shippingFee.toFixed(2)}`;
}

console.log(calculateShipping(0.3, "local"));
console.log(calculateShipping(1.5, "overseas"));
console.log(calculateShipping(12, "local"));
console.log(calculateShipping(2, "invalid"));
