function Character(name, classType, level){
    this.name = name
    this.classType = classType
    this.level = level
    this.damage = level
    this.health = level
    this.bag = []

    this.dodge = () =>{
        return `${this.name} dodge the attack!`
    }

    this.faint = () =>{
        if(this.health === 0){
            return `${this.name} has fainted!`
        } else {
            return `${this.name} can still fight!`
        }
    }

    this.attack = (enemy) =>{
        enemy.health -= this.damage
        return `${this.name} attacked the enemy with ${this.damage} damage`
    }
}

let monster = {
    type: "Goblin",
    level: 6,
    damage: 8,
    health: 15,
    attack: (character) => {
        const dodge = Boolean(Math.round(Math.random()));
        if(dodge){
            return character.dodge()
        } else {
            character.health -= monster.damage
            return `${monster.type} attacked ${character.name} with ${monster.damage} damage! your health is now ${character.health}`
        }
    },
    takeDamage: () => {
        return `${monster.type} took damaged! health is now ${monster.health}`
    }
}

let character = new Character("Guts", "Berserker", 8)

console.log(character.attack(monster));
console.log(monster.takeDamage());
console.log(monster.attack(character));
console.log(character.faint());

if(character.health > 0) {
    console.log(character.attack(monster));
    if(monster.health <= 0) {
        console.log(`${character.name} defeated ${monster.type}`);
    } else {
        console.log(monster.attack(character));
    }
} else {
    console.log("Game Over");
}


function Merchant(items){
    this.items = items

    this.buy = (item, character) =>{
        character.bag.push(item)
        return `${character.name} bought ${item}`
    }

    this.sell = (item, character) => {
        if(character.bag.includes(item)){
            let index = character.bag.indexOf(item);
            character.bag.splice(index, 1);
            return character.name + " sold a " + item + "!"
            
        } else {
            return character.name + " does not have a " + item + " to sell."
        }
    }
}

let merchant = new Merchant(["potion", "sword"])

console.log(merchant.buy("sword", character));
console.log(merchant.sell("sword", character));
console.log(merchant.sell("potion", character));