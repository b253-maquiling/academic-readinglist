const mongoose = require("mongoose");

const movieSchema = new mongoose.Schema({
  movieTitle: {
    type: String,
    required: [true, "Movie title is required"],
  },
  yearReleased: {
    type: String,
    required: [true, "Year release is required"],
  },
  rating: {
    type: Number,
    required: [true, "Rating is required"],
  },
  review: {
    type: String,
    required: [true, "Review is required"],
  },
});

module.exports = mongoose.model("Movie", movieSchema);
