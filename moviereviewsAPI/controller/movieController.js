const asyncHandler = require("express-async-handler");
const Movie = require("../model/movieModel");

// @desc Get movie reviews
// @route GET /movie-reviews/
// @access public
const getMovies = asyncHandler(async (req, res) => {
  const movies = await Movie.find();

  if (movies.length <= 0) {
    return res.status(400).json({ noMovies: "Please input more movies" });
  }

  return res.status(200).json(movies);
});

// @desc Create movie review
// @route POST /movie-reviews/
// @access public
const createMovie = asyncHandler(async (req, res) => {
  const { movieTitle, yearReleased, rating, review } = req.body;

  const duplicateMovieTitle = await Movie.findOne({ movieTitle: movieTitle });

  if (!movieTitle || !yearReleased || !rating || !review) {
    return res.status(400).send("All fields are mandatory!");
  }

  if (duplicateMovieTitle) {
    return res.status(400).send(`${movieTitle} has already been added`);
  }

  const movie = await Movie.create({
    movieTitle,
    yearReleased,
    rating,
    review,
  });

  return res.status(201).json(movie);
});

// @desc Update movie review
// @route PUT /movie-reviews/
// @access public
const updateMovie = asyncHandler(async (req, res) => {
  const MovieTitle = await Movie.findOne({ movieTitle: req.body.movieTitle });

  if (!MovieTitle) {
    return res.status(404).send(`${req.body.movieTitle} is not in the list`);
  }

  const updatedMovie = await Movie.findOneAndUpdate(
    { movieTitle: req.body.movieTitle },
    req.body,
    { new: true }
  );

  return res.status(201).json(updatedMovie);
});

// @desc Delete movie review
// @route DELETE /movie-reviews/
// @access public
const deleteMovie = asyncHandler(async (req, res) => {
  const MovieTitle = await Movie.findOne({ movieTitle: req.body.movieTitle });

  if (!MovieTitle) {
    return res.status(404).send(`${req.body.movieTitle} is not in the list`);
  }

  const movie = await Movie.deleteOne({ movieTitle: req.body.movieTitle });
  return res.status(201).json(movie);
});

module.exports = { getMovies, createMovie, updateMovie, deleteMovie };
