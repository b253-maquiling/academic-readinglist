const express = require("express");
const {
  getMovies,
  createMovie,
  updateMovie,
  deleteMovie,
} = require("../controller/movieController");

const router = express.Router();

router.get("/", getMovies);

router.post("/", createMovie);

router.put("/", updateMovie);

router.delete("/", deleteMovie);

module.exports = router;
